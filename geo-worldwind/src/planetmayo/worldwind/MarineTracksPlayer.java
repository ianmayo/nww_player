package planetmayo.worldwind;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.layers.Layer;

import java.util.Date;

public class MarineTracksPlayer {
	
	private static Date beginning=null, current=null, end=null;
	
	/**
	 * When a new set of data is added to be played, check if its beginning is 
	 * earlier then the current beginning.
	 * @param d
	 */
	public static void NewBeginning(WorldWindow wwd, Date d)
	{
		if(beginning == null || d.before(beginning))
			beginning = new Date(d.getTime());
		if(current == null)
		{
			current = new Date(d.getTime());
//			if(beginning != null && end != null)
//				Advance(wwd);
		}
	}
	
	/**
	 * When a new set of data is added to be played, check if its end is 
	 * later then the current end.
	 * @param d
	 */
	public static void NewEnd(WorldWindow wwd, Date d)
	{
		if(end == null || d.after(end))
			end = new Date(d.getTime());
//		if(current == null && beginning != null && end != null)
//		{
//			current = beginning;
//			Advance(wwd);
//		}
	}

	public static void Advance(WorldWindow wwd)
	{
		if(current != null)
		{
			current.setTime(current.getTime() + 1000 * 60 * 5);
			if(current.after(end))
				current.setTime(end.getTime());
			System.out.print(current + "\n");
			for (Layer layer : wwd.getModel().getLayers())
			{
				if(layer instanceof TimeSpanning)
				{
					((TimeSpanning) layer).setTime(current);
				}
			}
			wwd.redraw();
		}
	}
	
	public static void Rewind(WorldWindow wwd)
	{
		if(current != null)
		{
			current.setTime(current.getTime() - 1000 * 60 * 5);
			if(current.before(beginning))
				current.setTime(beginning.getTime());
			System.out.print(current + "\n");
			for (Layer layer : wwd.getModel().getLayers())
			{
				if(layer instanceof TimeSpanning)
				{
					((TimeSpanning) layer).setTime(current);
				}
			}
		}
		wwd.redraw();
	}
}

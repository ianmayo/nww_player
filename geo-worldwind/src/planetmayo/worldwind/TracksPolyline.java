package planetmayo.worldwind;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Polyline;

import java.awt.Color;
import java.util.ArrayList;

import javax.media.opengl.GL;

/**
 * extend Polyline to 'hardcode' the line features we want, as well as 
 * disabling depth testing so that the line is displayed no matter what is
 * 'in front' of it.
 * @author Wellspring
 *
 */
public class TracksPolyline extends Polyline
{
	TracksPolyline(final ArrayList<Position> positions, final Color color)
	{
		super(positions);
		setFollowTerrain(false);
		setClosed(false);
		setPathType(Polyline.LINEAR);
		setLineWidth(3);
		setColor(color);
	}

	//need to disable opengl depth test so that tracks lines are always shown 
	//regardless of something being in front of them.
	public void render(DrawContext dc)
	{
		dc.getGL().glDisable(GL.GL_DEPTH_TEST);
		super.render(dc);
		dc.getGL().glEnable(GL.GL_DEPTH_TEST);
	}
}

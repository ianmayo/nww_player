package planetmayo.worldwind;

import java.util.Date;

/**
 * Represents anything that changes based upon time.
 * I dont feel that this is a good name, but can't think of anything better.
 * @author Wellspring
 *
 */
public interface TimeSpanning 
{
	
	/**
	 * 
	 * @param time
	 */
	public void setTime(Date time);
	
	public Date getBeginning();
	
	public Date getEnd();
}

package planetmayo.worldwind;

import gov.nasa.worldwind.WorldWindow;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JSlider;

public class TracksPanel extends JPanel {

	private WorldWindow wwd;
	
	/**
	 * generated
	 */
	private static final long serialVersionUID = 2626573673318555685L;

	/*
	 * I don't like passing wwd to the constructor, but it follows the construct of worldwind.
	 */
	public TracksPanel(WorldWindow wwd)
	{
		super(new FlowLayout());
		this.wwd = wwd;
		setName("Tracks");
		setPreferredSize(new Dimension(200, 600));
		addComponents();
	}
	
	private void addComponents()
	{
		
		JSlider timeSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
		//disable user from being able to move slider
		timeSlider.removeMouseListener(timeSlider.getMouseListeners()[0]);
		timeSlider.setPreferredSize(new Dimension(175, 20));
		add(timeSlider);
		
		Button backwards = new Button("<");
		backwards.setPreferredSize(new Dimension(26, 20));
		backwards.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				MarineTracksPlayer.Rewind(wwd);
			}
		});
		add(backwards);
		
		Label invis = new Label("");
		invis.setPreferredSize(new Dimension(50, 20));
		add(invis);
		
		Button forwards = new Button(">");
		forwards.setPreferredSize(new Dimension(26, 20));
		forwards.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				MarineTracksPlayer.Advance(wwd);
			}
		});
		add(forwards);
		
		Button fitToWindow = new Button("Fit to Window");
		fitToWindow.setPreferredSize(new Dimension(100, 20));
		fitToWindow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				RenderMarineTracks.AppFrame.ZoomToTracks(wwd);
			}
		});
		add(fitToWindow);
	}
}

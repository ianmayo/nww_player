package planetmayo.worldwind;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class MarineTracksLayer extends RenderableLayer implements TimeSpanning
{

	private Sector sector;
	final private String		_myName;
	private Material _myColor = null;
	private ArrayList<MarinePosition> markers;
	private ArrayList<Position> positions = new ArrayList<Position>();
    PointPlacemark shipMarker;
	
	//is there a better way to pass this constructor an array of marinePositions, and then call super() with that same array?
	public MarineTracksLayer(ArrayList<MarinePosition> markers, String shipName)//, final Material myColor)
	{
		super();
		_myName = shipName;
		
		this.markers = markers;
		Collections.sort(this.markers);
		
		//calculate sector and get positions and color
		double minLat=90, maxLat=-90, minLon=180, maxLon=-180;
		for(MarinePosition m : markers)
		{
			//technically don't need to do this because markerRenderer renders 
			//the markers, not RenderableLayer
			addRenderable(m);
			Position p = m.getPosition();
			positions.add(p);
			if(_myColor == null)
				_myColor = ((PointPlacemarkAttributes)m.getAttributes()).getLineMaterial();
			minLat = Math.min(minLat, p.getLatitude().degrees);
			maxLat = Math.max(maxLat, p.getLatitude().degrees);
			minLon = Math.min(minLon, p.getLongitude().degrees);
			maxLon = Math.max(maxLon, p.getLongitude().degrees);
		}
		sector = new Sector(Angle.fromDegrees(minLat), Angle.fromDegrees(maxLat), Angle.fromDegrees(minLon), Angle.fromDegrees(maxLon));
		TracksPolyline polyline = new TracksPolyline(getMarkerPositions(), getColor().getAmbient());
		addRenderable(polyline);
		
//		shipMarker = new PointPlacemark(markers.get(0).getPosition());
//		shipMarker.setLabelText("Placemark H");
//		shipMarker.setValue(AVKey.DISPLAY_NAME, "Relative to ground, Blue label, Magenta point and line, Scale 10");
//		shipMarker.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
//		shipMarker.setLineEnabled(true);
//		PointPlacemarkAttributes attrs = new PointPlacemarkAttributes();
//        attrs.setLabelColor("ffff0000");
//        attrs.setLineMaterial(Material.MAGENTA);
//        attrs.setLineWidth(2d);
//        attrs.setUsePointAsDefaultImage(true);
//        attrs.setScale(10d);
//        shipMarker.setAttributes(attrs);
//		this.addRenderable(shipMarker);
	}
	
	public Sector getSector()
	{
		return sector;
	}
	
	public Material getColor()
	{
		return _myColor;
	}

	public String getName()
	{
		return _myName;
	}

	public ArrayList<Position> getMarkerPositions()
	{
		return positions;
	}

	@Override
	public void setTime(Date time) 
	{
		if(shipMarker != null)
			shipMarker.setHighlighted(false);
		MarinePosition last = null, next = null;
		for(MarinePosition marker: markers)
		{
			last = marker;
			if(marker.getDatetime().after(time))
			{
				next = marker;
				break;
			}
		}
		if(last != null && next != null)
		{
			long currentTime = time.getTime();
			long lastDif = Math.abs(last.getDatetime().getTime() - currentTime);
			long nextDif = Math.abs(next.getDatetime().getTime() - currentTime);
			if(lastDif < nextDif)
				shipMarker = last;
			else
				shipMarker = next;
		}
		else if(last != null)
			shipMarker = last;
		else if(next != null)
			shipMarker = next;
		else
			return;
		shipMarker.setHighlighted(true);
	}

	@Override
	public Date getBeginning() 
	{
		return markers.get(0).getDatetime();
	}

	@Override
	public Date getEnd()
	{
		return markers.get(markers.size()-1).getDatetime();
	}
}

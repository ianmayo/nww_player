package planetmayo.worldwind;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;

public class RenderMarineTracksTest
{
	@Test
	public void loadReplayTracks()
	{
		RenderMarineTracks.AppFrame tracks = new RenderMarineTracks.AppFrame();
		Map<String, ArrayList<MarinePosition>> boadTracks = tracks.loadReplayData(new File("data/boat.REP"));
		assertEquals(2, boadTracks.size());
	}
}

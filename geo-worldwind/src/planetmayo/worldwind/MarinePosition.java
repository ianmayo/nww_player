package planetmayo.worldwind;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;
import gov.nasa.worldwind.util.OGLStackHandler;

import java.awt.Color;
import java.util.Date;

import javax.media.opengl.GL;

public class MarinePosition extends PointPlacemark implements Comparable<MarinePosition>
{
	//yymmdd, hhMMss, ship_name, lat-degs, long-degs, heading-degs, speed-knots, depth-m
	private Date datetime;
	private String shipName;
	private double latitudeDegrees;
	private double longitudeDegrees;
	private double headingDegrees;
	private double speedKnots;
	private double depthMeters;
	
//	private final int MarkerPixels = 3;
//	private final int MarkerPixelsPicked = 6;
//	private Material material, pickedMaterial;

    protected PickSupport pickSupport = new PickSupport();
	
	public MarinePosition(Date d, String ship, double lat, double lon, double heading, double speed, double depth, Color color)
	{
		super(new Position(Angle.fromDegrees(lat), Angle.fromDegrees(lon), depth));
		setAltitudeMode(WorldWind.ABSOLUTE);
		setLineEnabled(false);
		setEnableBatchPicking(true);
		setEnableBatchRendering(true);
		PointPlacemarkAttributes attr = new PointPlacemarkAttributes();
//		attr.setLabelColor("ffff0000");
		attr.setUsePointAsDefaultImage(true);
		attr.setScale(10d);
		//its strange, but setting line color is the same as setting the point color
        attr.setLineColor(getHexColorString(color));
		setAttributes(attr);

		PointPlacemarkAttributes hAttr = new PointPlacemarkAttributes();
//		attr.setLabelColor("ffff0000");
		hAttr.setUsePointAsDefaultImage(true);
		hAttr.setScale(20d);
		//its strange, but setting line color is the same as setting the point color
		hAttr.setLineColor(getHexColorString(color));
		setHighlightAttributes(hAttr);
		
		datetime = d;
		shipName = ship;
		latitudeDegrees = lat;
		longitudeDegrees = lon;
		headingDegrees = heading;
		speedKnots = speed;
		depthMeters = depth;
//		pickedMaterial = Material.WHITE;
	}
	
	private String getHexColorString(Color color)
	{
		String hexAlpha = Integer.toHexString(color.getAlpha());
		if(hexAlpha.length() < 2)
			hexAlpha = "0" + hexAlpha;
		String hexBlue = Integer.toHexString(color.getBlue());
		if(hexBlue.length() < 2)
			hexBlue = "0" + hexBlue;
		String hexGreen = Integer.toHexString(color.getGreen());
		if(hexGreen.length() < 2)
			hexGreen = "0" + hexGreen;
		String hexRed = Integer.toHexString(color.getRed());
		if(hexRed.length() < 2)
			hexRed = "0" + hexRed;
		return "0x" + hexAlpha + hexBlue + hexGreen + hexRed;
	}

	public Date getDatetime() {
		return datetime;
	}

	public String getShipName() {
		return shipName;
	}

	public double getLatitudeDegrees() {
		return latitudeDegrees;
	}

	public double getLongitudeDegrees() {
		return longitudeDegrees;
	}

	public double getHeadingDegrees() {
		return headingDegrees;
	}

	public double getSpeedKnots() {
		return speedKnots;
	}

	public void setSpeedKnots(double speedKnots) {
		this.speedKnots = speedKnots;
	}

	public double getDepthMeters() {
		return depthMeters;
	}
	
	/**
	 * playing around with deep picking and batch rendering
	 */
	@Override
    public void render(DrawContext dc)
    {
    	dc.setDeepPickingEnabled(false);
    	super.render(dc);
    }
	
	/**
	 * Override this function to remove enabling of depth test.
	 */
	@Override
	protected void drawPoint(DrawContext dc, PickSupport pickCandidates)
    {
        javax.media.opengl.GL gl = dc.getGL();
        boolean depthEnabled = gl.glIsEnabled(GL.GL_DEPTH_TEST);
        gl.glDisable(GL.GL_DEPTH_TEST);

        OGLStackHandler osh = new OGLStackHandler();
        try
        {
            osh.pushAttrib(gl, GL.GL_POINT_BIT);

            this.setLineColor(dc, pickCandidates);
            this.setPointSize(dc);

            // The point is drawn using a parallel projection.
            osh.pushProjectionIdentity(gl);
            gl.glOrtho(0d, dc.getView().getViewport().width, 0d, dc.getView().getViewport().height, -1d, 1d);

            osh.pushModelviewIdentity(gl);

            // Suppress any fully transparent pixels.
            gl.glEnable(GL.GL_ALPHA_TEST);
            gl.glAlphaFunc(GL.GL_GREATER, 0.001f);

            // Adjust depth of point to bring it slightly forward
            double depth = this.screenPoint.z - (8d * 0.00048875809d);
            depth = depth < 0d ? 0d : (depth > 1d ? 1d : depth);
            gl.glDepthFunc(GL.GL_LESS);
            gl.glDepthRange(depth, depth);

            gl.glBegin(GL.GL_POINTS);
            gl.glVertex3d(this.screenPoint.x, this.screenPoint.y, 0);
            gl.glEnd();

            gl.glDepthRange(0, 1); // reset depth range to the OGL default

            if (!dc.isPickingMode()) // don't pick via the label
                this.drawLabel(dc);
        }
        finally
        {
            osh.pop(gl);
        }
        if(depthEnabled)
        	gl.glEnable(GL.GL_DEPTH_TEST);
    }

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * Implemented so that tracks can be ordered chronologically,
	 * specifically using the Collections.sort function.
	 */
	@Override
	public int compareTo(MarinePosition other) {
		Date d = other.getDatetime();
		if(datetime.before(d))
			return -1;
		else
			return 1;
	}
}

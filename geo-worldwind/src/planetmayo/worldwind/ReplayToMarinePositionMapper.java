package planetmayo.worldwind;

import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.markers.BasicMarkerAttributes;
import gov.nasa.worldwind.render.markers.BasicMarkerShape;
import gov.nasa.worldwind.render.markers.MarkerAttributes;
import gov.nasa.worldwind.util.Logging;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <ol>
 * <li>Date, either 2 of 4 figure date, followed by month then date</li>
 * <li>Time</li>
 * <li>Vessel Name - either as single, unquoted word, or as a multi-word phrase
 * enclosed in quotation marks.</li>
 * <li>Symbology</li>
 * <li>Latitude Degrees (Debrief is able to handle decimal degrees - provide
 * zeros for mins and secs)</li>
 * <li>Latitude Minutes (Debrief is able to handle decimal mins - provide zeros
 * for secs)</li>
 * <li>Latitude Seconds</li>
 * <li>Latitude Hemisphere</li>
 * <li>Longitude Degrees (Debrief is able to handle decimal degrees - provide
 * zeros for mins and secs)</li>
 * <li>Longitude Minutes (Debrief is able to handle decimal mins - provide zeros
 * for secs)</li>
 * <li>Longitude Seconds</li>
 * <li>Longitude Hemisphere</li>
 * <li>Heading (0..359.9 degrees)</li>
 * <li>Speed (knots)</li>
 * <li>Depth (metres)</li>
 * </ol>
 */
public final class ReplayToMarinePositionMapper
{
	private final Map<String, MarkerAttributes> vesselNameAndSymbologyToMarkerAttributes = new HashMap<String, MarkerAttributes>();
	private static final Map<Character, Material> SYMBOL_COLOR_TO_MATERIAL_LOOKUP = new HashMap<Character, Material>();
	private final SimpleDateFormat dateFormat_yyMMdd = new SimpleDateFormat(
			"yyMMdd hhmmss.SSS");
	private final SimpleDateFormat dateFormat_yyyyMMdd = new SimpleDateFormat(
			"yyyyMMdd hhmmss.SSS");

	static
	{
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('@', Material.WHITE);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('A', Material.BLUE);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('B', Material.GREEN);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('C', Material.RED);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('D', Material.YELLOW);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('E', Material.CYAN);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('F', Material.ORANGE);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('G', Material.ORANGE);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('H', Material.CYAN);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('I', Material.GREEN);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('J', Material.YELLOW);
		SYMBOL_COLOR_TO_MATERIAL_LOOKUP.put('K', Material.PINK);
	}

	public MarinePosition map(String tokens[])
	{
		MarkerAttributes attrsForCurrentVessel = null;
		String rawVesselName = parseVesselName(tokens);
		String rawSymbology = tokens[3];
		String vesselNameAndSymbology = rawVesselName + "-" + rawSymbology;

		if (!vesselNameAndSymbologyToMarkerAttributes
				.containsKey(vesselNameAndSymbology))
		{
			// create marker attributes only once per vessel though they are
			// repeated for each line in the file
			attrsForCurrentVessel = parseSymbology(tokens);
			vesselNameAndSymbologyToMarkerAttributes.put(vesselNameAndSymbology,
					attrsForCurrentVessel);
		}
		else
		{
			attrsForCurrentVessel = vesselNameAndSymbologyToMarkerAttributes
					.get(vesselNameAndSymbology);
		}
		double latDegs = Double.valueOf(tokens[4]);
		double latMins = Double.valueOf(tokens[5]);
		double latSecs = Double.valueOf(tokens[6]);
		String latHemi = tokens[7];
		double lonDegs = Double.valueOf(tokens[8]);
		double lonMins = Double.valueOf(tokens[9]);
		double lonSecs = Double.valueOf(tokens[10]);
		String longHemi = tokens[11];

		latDegs = latDegs + latMins / 60d + latSecs / (60d * 60d);
		lonDegs = lonDegs + lonMins / 60d + lonSecs / (60d * 60d);

		if (latHemi.toLowerCase().equals("s"))
			latDegs = -latDegs;
		if (longHemi.toLowerCase().equals("w"))
			lonDegs = -lonDegs;

		// TODO map to heading Double heading = Double.valueOf(tokens[12]);
		Double speed = Double.valueOf(tokens[13]);
		Double depth = Double.valueOf(tokens[13]);
		Date dateTime = null;
		try
		{
			dateTime = parseDateTime(tokens);
		}
		catch (ParseException e)
		{
			Logging.logger().warning(e.getMessage());
		}

		return new MarinePosition(dateTime, rawVesselName, latDegs, lonDegs, -1,
				speed, depth, attrsForCurrentVessel.getMaterial().getAmbient());
	}

	public String parseVesselName(String[] tokens)
	{
		// Vessel Name - either as single, unquoted word, or as a multi-word
		// phrase enclosed in quotation marks.
		return tokens[2].replaceAll("\"", "");
	}

	public MarkerAttributes parseSymbology(String[] tokens)
	{
		tokens[3].charAt(0);
		char colorIndicator = tokens[3].charAt(1);
		/*
		 * TODO There is a gap in mapping Symbology column to BasicMarkerShape.
		 * Symbology has lot of options than supported in BasicMarkerShape. For now
		 * default to Sphere
		 */

		return new BasicMarkerAttributes(
				SYMBOL_COLOR_TO_MATERIAL_LOOKUP.get(colorIndicator),
				BasicMarkerShape.SPHERE, 1d);
	}

	public Date parseDateTime(String[] values) throws ParseException
	{
		if (values[0].length() == 6)
		{
			return dateFormat_yyMMdd.parse(values[0] + " " + values[1]);
		}
		else if (values[0].length() == 8)
		{
			return dateFormat_yyyyMMdd.parse(values[0] + " " + values[1]);
		}
		return null;
	}
}

package planetmayo.worldwind;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.SurfaceImageLayer;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import gov.nasa.worldwindx.examples.ApplicationTemplate;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class RenderMarineTracks extends ApplicationTemplate
{

	public static class AppFrame extends ApplicationTemplate.AppFrame
	{
		private static final long serialVersionUID = 1L;

		private static final Color[] theCols = new Color[]
		{ Color.RED, Color.YELLOW, Color.ORANGE };
		
		private TracksPanel tracksPanel;

		public AppFrame()
		{
			setJMenuBar(createJMenuBar());
			tracksPanel = new TracksPanel(getWwd());
			this.getContentPane().add(tracksPanel, BorderLayout.EAST);
			// increasing vertical exaggeration improves the appearance of the
			// ocean surface layer
			this.getWwd().getSceneController().setVerticalExaggeration(10d);
			createOceanSurfaceLayer();
		}
		
		private void createOceanSurfaceLayer()
		{
			//need to come up with a better way of dividing the earth into 
			//sectors because the surface shapes don't meet very well.
			TexturedLayer northHem = new TexturedLayer("data/ocean.png", new Sector(
					Angle.ZERO, Angle.POS90, Angle.NEG180, Angle.POS180));
			northHem.setElevation(0);
			//increasing density improves the accuracy of the ocean layer 
			//along the coast.  However, the more density, the slower the layer
			//loads.
			northHem.setDensity(512);
			TexturedLayer southHem = new TexturedLayer("data/ocean.png", new Sector(
					Angle.NEG90, Angle.ZERO, Angle.NEG180, Angle.POS180));
			southHem.setElevation(0);
			southHem.setDensity(512);
			SurfaceImageLayer oceanLayer = new SurfaceImageLayer();
			oceanLayer.addRenderable(northHem);
			oceanLayer.addRenderable(southHem);
			oceanLayer.setName("Ocean Surface");
			insertBeforePlacenames(getWwd(), oceanLayer);
			this.getLayerPanel().update(this.getWwd());
		}

		
		/**
		 * consider moving this function to RenderMarineTracks
		 * @param wwd
		 */
		public static void ZoomToTracks(WorldWindow wwd)
		{
			double totalLat=0, totalLon=0, count=0;
			for (Layer layer : wwd.getModel().getLayers().getLayersByClass(MarineTracksLayer.class))
			{
				LatLon center = ((MarineTracksLayer) layer).getSector().getCentroid();
				totalLat += center.latitude.degrees;
				totalLon += center.longitude.degrees;
				count++;
			}
			if(count > 0)
			{
				// should come up with 'smart' values for first parameter and
				// elevation.  there may be a way to derive a suitable elevation from the sectors
				BasicOrbitView view = (BasicOrbitView) wwd.getView();
				view.addEyePositionAnimator(2000, view.getEyePosition(), new Position(
						new LatLon(Angle.fromDegrees(totalLat/count), Angle.fromDegrees(totalLon/count)), 60000));
			}
		}

		private void loadTracks(File tracksFile)
		{
			// find out what type of data file it is
			String fileName = tracksFile.getName();
			String suffix = fileName.substring(fileName.lastIndexOf('.'),
					fileName.length());

			// and load it
			final Map<String, ArrayList<MarinePosition>> theTracks;
			if (suffix.toLowerCase().equals(".rep"))
				theTracks = loadReplayData(tracksFile);
			else
				theTracks = loadTrialData(tracksFile);

//			Iterator<ArrayList<MarinePosition>> iter = theTracks.values().iterator();
			for(String shipName : theTracks.keySet())
			{
				ArrayList<MarinePosition> thisT = theTracks.get(shipName);
				// and put it into a marker-layer
				MarineTracksLayer layer = new MarineTracksLayer(thisT, shipName);
				MarineTracksPlayer.NewBeginning(getWwd(), layer.getBeginning());
				MarineTracksPlayer.NewEnd(getWwd(), layer.getEnd());
				// Add the layer to the model and update the layer panel.
				insertBeforePlacenames(getWwd(), layer);
				this.getLayerPanel().update(this.getWwd());
			}

			ZoomToTracks(getWwd());
		}

		public Map<String, ArrayList<MarinePosition>> loadReplayData(File tracksFile)
		{
			Map<String, ArrayList<MarinePosition>> result = new HashMap<String, ArrayList<MarinePosition>>();
			ReplayToMarinePositionMapper mapper = new ReplayToMarinePositionMapper();
			BufferedReader br = null;
			try
			{
				br = dataSupplier(tracksFile);
				String str;
				while ((str = br.readLine()) != null)
				{
					String tokens[] = str.split("\\s+");

					// do we have this track already?
					String vesselName = mapper.parseVesselName(tokens);
					MarinePosition newPoint = mapper.map(tokens);

					ArrayList<MarinePosition> thisTrack = result.get(vesselName);
					if (thisTrack == null)
					{
						// create the track
						thisTrack = new ArrayList<MarinePosition>();
						result.put(vesselName, thisTrack);
					}
					thisTrack.add(newPoint);
				}

			}
			catch (Exception e)
			{
				Logging.logger().warning(
						e.getMessage() + " while loading track data from "
								+ tracksFile.getAbsolutePath());
				// ignore for now
			}
			finally
			{
				if (br != null)
				{
					try
					{
						br.close();
					}
					catch (IOException ignore)
					{
					}
				}
			}
			return result;
		}

		private HashMap<String, ArrayList<MarinePosition>> loadTrialData(File tracksFile)
		{
			HashMap<String, ArrayList<MarinePosition>> res = new HashMap<String, ArrayList<MarinePosition>>();
			HashMap<String, Color> materials = new HashMap<String, Color>();
			BufferedReader br = null;
			try
			{
				br = dataSupplier(tracksFile);
				String str;
				while ((str = br.readLine()) != null)
				{
					String values[] = str.split(",");
					if (values.length != 8)
					{
						JOptionPane.showMessageDialog(this,
								"Invalid line in " + tracksFile.getPath() + ".");
						break;
					}
					// NOTE: doesn't account for values below 10 w/o a leading
					// zero
					String dateStr = values[0];
					if (dateStr.length() == 5)
					{
						dateStr = "0" + dateStr;
					}
					String timeStr = values[1];
					if (timeStr.length() == 5)
					{
						timeStr = "0" + timeStr;
					}

					int year = Integer.parseInt(dateStr.substring(0, 2));
					int month = Integer.parseInt(dateStr.substring(2, 4));
					int day = Integer.parseInt(dateStr.substring(4, 6));
					int hour = Integer.parseInt(timeStr.substring(0, 2));
					int minute = Integer.parseInt(timeStr.substring(2, 4));
					int second = Integer.parseInt(timeStr.substring(4, 6));
					// setup a java.util.date
					Calendar c = GregorianCalendar.getInstance();
					c.clear();
					c.set(year, month, day, hour, minute, second);
					Date datetime = c.getTime();
					// JOptionPane.showMessageDialog(this, datetime.toString());
					// //testing
					String shipName = values[2];
					double latitudeDegrees = Double.parseDouble(values[3]);
					double longitudeDegrees = Double.parseDouble(values[4]);
					double headingDegrees = Double.parseDouble(values[5]);
					double speedKnots = Double.parseDouble(values[6]);
					double depthMeters = Double.parseDouble(values[7]);

					// do we have this track already?
					ArrayList<MarinePosition> thisTrack = res.get(shipName);
					if (thisTrack == null)
					{
						// retrieve the next colour in the cycle
						Color thisCol = theCols[res.size() % theCols.length];
						materials.put(shipName, thisCol);

						// create teh track
						thisTrack = new ArrayList<MarinePosition>();
						res.put(shipName, thisTrack);
					}

					thisTrack.add(new MarinePosition(datetime, shipName, latitudeDegrees, longitudeDegrees,
							headingDegrees, speedKnots, depthMeters, materials.get(shipName)));
				}
			}
			catch (FileNotFoundException ex)
			{

			}
			catch (IOException ex)
			{

			}
			finally
			{
				if (br != null)
				{
					try
					{
						br.close();
					}
					catch (IOException ignore)
					{
					}
				}
			}
			return res;
		}

		private BufferedReader dataSupplier(File tracksFile)
				throws FileNotFoundException
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(tracksFile)));
			return br;
		}

		private JMenuBar createJMenuBar()
		{
			JMenu fileMenu = new JMenu("File");

			JMenuItem loadSampleFile = new JMenuItem("Load Sample file");
			loadSampleFile.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent e)
				{
					loadTracks(new File("data/tracks.txt"));
				}
			});
			fileMenu.add(loadSampleFile);

			JMenuItem loadTracksItem = new JMenuItem("Load Tracks File...");
			loadTracksItem.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent e)
				{
					JFileChooser c = new JFileChooser();
					// Demonstrate "Open" dialog:
					int rVal = c.showOpenDialog(AppFrame.this);
					if (rVal == JFileChooser.APPROVE_OPTION)
					{
						loadTracks(c.getSelectedFile());
					}
				}
			});
			fileMenu.add(loadTracksItem);

			JMenuItem exitItem = new JMenuItem("Exit");
			exitItem.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					AppFrame.this.dispose();
				}
			});
			fileMenu.add(exitItem);

			// JMenu playerMenu = new JMenu("Tracks Player");
			//
			// JMenuItem playTracksItem = new JMenuItem("Play Tracks File...");
			// playTracksItem.addActionListener(new ActionListener()
			// {
			// @Override
			// public void actionPerformed(ActionEvent e)
			// {
			// LayerList layers = AppFrame.this.getWwd().getModel().getLayers();
			// List<Layer> tracksLayers =
			// layers.getLayersByClass(MarineTracksLayer.class);
			// Object[] possibilities = tracksLayers.toArray();
			// Layer trackToPlay = (Layer)JOptionPane.showInputDialog(
			// AppFrame.this,
			// "Choose tracks file to play...",
			// "Play Tracks",
			// JOptionPane.PLAIN_MESSAGE,
			// null,
			// possibilities,
			// possibilities[0]);
			// //JOptionPane.showMessageDialog(AppFrame.this,
			// trackToPlay.getName());
			// //testing
			// //play tracks file
			// }
			// });
			// playerMenu.add(playTracksItem);

			JMenuBar menuBar = new JMenuBar();
			menuBar.add(fileMenu);
			// menuBar.add(playerMenu);

			return menuBar;
		}
	}

    public static void main(String[] args)
    {
        // Call the static start method like this from the main method of your derived class.
        // Substitute your application's name for the first argument.
        RenderMarineTracks.start("World Wind Application", AppFrame.class);
    }

}
